import * as http from 'http';
import * as mongoose from 'mongoose';
import {app} from "./app";
import { pathDB } from './config/database';
const PORT = 3000;

const server = http.createServer(app);
server.listen(PORT);

server.on('listening',   () => {
    console.info(`Server listening on port ${PORT}`);
    mongoose.connect(pathDB, {
        useNewUrlParser: true,
        useCreateIndex: true
    }).then(() => console.info('Database connected...'))
        .catch((err: any) => console.log(err));
});
