import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import {secretKey} from "../config/database";
import {Product, schema} from "../models/Product";
import * as mongoose from "mongoose";
import * as multer from 'multer';
import * as Joi from 'joi';

const product = express.Router();

let storage = multer.diskStorage({
    destination: function (req:express.Request, file: Express.Multer.File, cb) {
        cb(null, './uploads')
    },
    filename: function (req:express.Request, file: Express.Multer.File, cb) {
        cb(null, Date.now() + file.originalname)
    }
});

let upload = multer({ storage: storage });

product.post('/add', upload.single('photo'),(req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.json({
                message: 'Data not provided!'
            })
        } else {
            jwt.verify(token, secretKey, (err, valid) => {
                if (err) {
                    return res.json({
                        success: false,
                        message: 'User not valid'
                    })
                } else {
                    let newProduct: object = {
                        _id: new mongoose.Types.ObjectId(),
                        name: req.body.name,
                        price: req.body.price,
                        description: req.body.description,
                        category : req.body.categoryId
                    };
                    Product.create(newProduct)
                        .then(product => {
                            return res.json({
                                success: true,
                                message: 'Product was added',
                                data: product
                            })
                        });
                }
            });
        }
    });

});



product.put('/edit/:id', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Product.findByIdAndUpdate({_id: req.params.id}, req.body)
                .then(() => {
                    Product.findOne({_id: req.params.id})
                        .then(product => {
                            res.json({
                                success: true,
                                message: 'Product was changed',
                                data: product
                            })
                        });
                });
        }
    })
});


product.delete('/delete/:id', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Product.deleteOne({_id: req.params.id})
                .then(product => {
                    res.json({
                        success: true,
                        message: 'Product was removed',
                        data: product
                    })
                });
        }
    })
});

product.delete( '/delete', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Product.deleteMany({})
                .then(product => {
                    res.json({
                        success: true,
                        message: 'All Products was deleted',
                        data: product
                    })
                });
        }
    })
});


product.get( '/', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Product.findOne({})
                .then(product => {
                    res.json({
                        success: true,
                        message: 'This Product',
                        data: product
                    })
                });
        }
    })
});


product.get( '/:categoryId', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Product.find({category: req.params.categoryId})
                .populate('category')
                .then(product => {
                    res.json({
                        success: true,
                        message: 'All Products',
                        data: product
                    })
                });
        }
    })
});


product.get( '/:categoryId', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Product.find({category: req.params.categoryId})
                .populate('category')
                .then(product => {
                    res.json({
                        success: true,
                        message: 'All Products in this Category',
                        data: product
                    })
                });
        }
    })
});


product.get( '/:id', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Product.findOne({_id: req.params.id})
                .then(product => {
                    res.json({
                        success: true,
                        message: 'This Product',
                        data: product
                    })
                });
        }
    })
});


export { product }


