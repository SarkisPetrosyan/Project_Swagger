import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import {secretKey} from "../config/database";
import { Product } from "../models/Product";
import { Favourite } from "../models/Favourite";

const favourite = express.Router();

favourite.post('/add', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            let newFavourite: object = {
                user:req.body.userId,
                product:req.body.productId,
            };

            Favourite.create(newFavourite)
                .then(favourite => {
                    return res.json({
                        success: true,
                        message: 'Favourite was added',
                        data: favourite
                    })
                });
        }
    })
});


favourite.delete('/delete/:id', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Favourite.deleteOne({_id: req.params.id})
                .then(favourite => {
                    res.json({
                        success: true,
                        message: 'Favourite was removed',
                        data: favourite
                    })
                });
        }
    })
});

favourite.delete( '/delete', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Favourite.deleteMany({})
                .then(favourite => {
                    res.json({
                        success: true,
                        message: 'All Favourites was deleted',
                        data: favourite
                    })
                });
        }
    })
});


favourite.get( '/', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Favourite.find({})
                .then(favourite => {
                    res.json({
                        success: true,
                        message: 'This Favourite',
                        data: favourite
                    })
                });
        }
    })
});

favourite.get( '/user/:userId', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Favourite.find({user: req.params.userId})
                .populate('user')
                .then(favourite => {
                    res.json({
                        success: true,
                        message: "User's favourites",
                        data: favourite
                    })
                });
        }
    })
});


favourite.get( '/product/:productId', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Favourite.find({product: req.params.productId})
                .populate('product')
                .then(favourite => {
                    res.json({
                        success: true,
                        message: "Product's favourites",
                        data: favourite
                    })
                });
        }
    })
});

export { favourite }


