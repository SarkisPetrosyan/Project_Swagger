import * as express from 'express';
import * as jwt from 'jsonwebtoken';
const user = express.Router();
import {User, addUser, logUser, comparePassword, getUserById, schema} from '../models/User';
import { secretKey } from '../config/database';
import * as mongoose from "mongoose";
import * as Joi from "joi";

user.post('/register', (req: express.Request, res: express.Response) => {
    let UserData: object = {
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        username: req.body.username,
        age: req.body.age,
        email: req.body.email,
        password: req.body.password
    };
    let newUser = new User(UserData);
    Joi.validate(req.body, schema, async(err, value) => {
        if (err) {
            res.json({
                message: 'Data not provided!'
            })
        } else {
            await addUser(newUser, (err, user) => {
                if (err) {
                    let message = '';
                    if (err.errors.username) message = 'Username is already token. ';
                    if (err.errors.email) message += 'Email already exists.';
                    return res.json({
                        success: false,
                        message
                    });
                }
                return res.json({
                    success: true,
                    message: 'User registration is successful.',
                    data: user
                });
            });
        }
    })
});


user.post('/login',  (req: express.Request, res: express.Response) => {
    const username: string = req.body.username;
    const password: string = req.body.password;
    Joi.validate(req.body, schema, async(err, value) => {
        if (err) {
            res.json({
                message: 'Data not provided!'
            })
        } else {
            await logUser(username, async (err, user) => {
                try {
                    await comparePassword(password, user.password, (err, isMatch) => {
                        if (err) throw err;
                        if (isMatch) {
                            const token = jwt.sign({
                                    data: {
                                        _id: user._id,
                                        username: user.username,
                                        name: user.name,
                                        email: user.email,
                                        contact: user.contact
                                    }
                                },
                                secretKey,
                                {
                                    expiresIn: 604800
                                }
                            );
                            return res.json({
                                success: true,
                                token
                            });
                        } else {
                            return res.json({
                                success: false,
                                token: 'Wrong Password.'
                            });
                        }
                    });
                } catch {
                    if (err) throw err;
                    if (!user) {
                        return res.json({
                            success: false,
                            message: "User not found!"
                        });
                    }
                }
            })
        }
    })
});


user.get('/:id', async (req: express.Request, res: express.Response) => {
    const _id: number = req.params.id;
    await getUserById(_id, (err, data) => {
        if (err) {
            return res.json({
                success: false,
                message: 'Data not found'
            })
        } else {
            return res.json({
                success: true,
                message: data
            })
        }
    })
});

export { user }