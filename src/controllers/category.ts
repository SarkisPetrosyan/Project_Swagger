import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import {secretKey} from "../config/database";
import { Category, schema } from "../models/Category";
import * as mongoose from "mongoose";
import * as Joi from "joi";

const category = express.Router();

category.get( '/', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Category.find({})
                .then(category => {
                    res.json({
                        success: true,
                        message: 'All Categories',
                        data: category
                    })
                });
        }
    })
});




category.post('/add', (req: express.Request, res: express.Response) => {
    let newCategory: object = {
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        description: req.body.description
    };
    const token = req.get('authorization');
    Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.json({
                message: 'Data not provided!'
            })
        } else {
            jwt.verify(token, secretKey, (err, valid) => {
                if (err) {
                    return res.json({
                        success: false,
                        message: 'User not valid'
                    })
                } else {
                    Category.create(newCategory)
                        .then(category => {
                            return res.json({
                                success: true,
                                message: 'Category was added',
                                data: category
                            })
                        });
                }
            })
        }
    })
});

category.put('/edit/:id', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Category.findByIdAndUpdate({_id: req.params.id}, req.body)
                .then(() => {
                    Category.findOne({_id: req.params.id})
                        .then(category => {
                            res.json({
                                success: true,
                                message: 'Category was changed',
                                data: category
                            })
                        });
                });
        }
    })
});


category.delete('/delete/:id', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Category.deleteOne({_id: req.params.id})
                .then(category => {
                    res.json({
                        success: true,
                        message: 'Category was removed',
                        data: category
                    })
                });
        }
    })
});

category.delete( '/delete', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Category.deleteMany({})
                .then(category => {
                    res.json({
                        success: true,
                        message: 'All Categories was deleted',
                        data: category
                    })
                });
        }
    })
});


category.get( '/:id', (req: express.Request, res: express.Response) => {
    const token = req.get('authorization');
    jwt.verify(token, secretKey, (err, valid) => {
        if (err) {
            return res.json({
                success: false,
                message: 'User not valid'
            })
        } else {
            Category.findOne({_id: req.params.id})
                .populate('products').exec((err, product) => {
                    if (err) {
                        res.json({
                            message: 'error in relation'
                        })
                    } else {
                        res.json({
                            Products: product
                        })
                    }
            })
        }
    })
});



export { category }