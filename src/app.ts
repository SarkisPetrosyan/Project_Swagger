import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { user } from './controllers/user';
import { product } from "./controllers/product";
import {category} from "./controllers/category";
import {favourite} from "./controllers/favourite";

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded( {extended: true}));
app.use(bodyParser.json());

app.use('/user', user);
app.use('/product', product);
app.use('/category', category);
app.use('/favourite', favourite);

export { app };