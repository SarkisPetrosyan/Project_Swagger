import * as mongoose from 'mongoose';
import * as unique from 'mongoose-unique-validator';
import * as bcrypt from 'bcryptjs';
import {Schema} from "mongoose";
import * as Joi from "joi";

interface userInterface {
    _id: string;
    name: string;
    username: string;
    age: number;
    email: string;
    password: string;
}

const userSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    age: {
        type: Number,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
});

userSchema.plugin(unique);

const User = mongoose.model('users', userSchema);

const getUserById = function (id, callback) {
    User.findById(id, callback);
};

const logUser = function (username, callback) {
    const query = {
        username: username
    };
    User.findOne(query, callback);
};


const addUser = function (newUser, callback) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
};


const comparePassword = function (password, hash, callback) {
    bcrypt.compare(password, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
};

export {
    userInterface,
    User,
    getUserById,
    logUser,
    addUser,
    comparePassword,
    schema
}

const schema = Joi.object().keys({
    name: Joi.string().min(3).max(30).required(),
    username: Joi.string().alphanum().min(3).max(30).required(),
    age: Joi.number().min(1).max(2).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }),
    password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
});