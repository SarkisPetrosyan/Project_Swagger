import * as mongoose from 'mongoose';
import * as unique from 'mongoose-unique-validator';
import {Schema} from "mongoose";
import * as Joi from "joi";

const categorySchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    },
});

categorySchema.plugin(unique);

const Category = mongoose.model('categories', categorySchema);

export { Category, schema }

const schema = Joi.object().keys({
    name: Joi.string().min(3).max(30).required(),
    description: Joi.string().min(1).max(50).required()
});