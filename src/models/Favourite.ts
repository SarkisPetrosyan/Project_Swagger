import * as mongoose from 'mongoose';
import {Schema} from "mongoose";

const favouriteSchema = new mongoose.Schema({
    user: { type: Schema.Types.ObjectId, ref: 'users' },
    product: { type: Schema.Types.ObjectId, ref: 'products' }
});


const Favourite = mongoose.model('favourites', favouriteSchema);

export { Favourite }