import * as mongoose from 'mongoose';
import {Schema} from "mongoose";
import * as Joi from 'joi';

const productSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category: { type: Schema.Types.ObjectId, ref: 'categories' }
});


const Product = mongoose.model('products', productSchema);

export { Product, schema }


const schema = Joi.object().keys({
    name: Joi.string().min(3).max(30).required(),
    price: Joi.string().min(2).max(6).required(),
    description: Joi.string().min(1).max(50).required()
});